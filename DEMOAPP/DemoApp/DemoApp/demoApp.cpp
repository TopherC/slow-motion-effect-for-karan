#include <string>
#include <math.h>
#include <stdint.h>
#include <tchar.h>
#include <stdio.h> 
#include <stdlib.h> 
#include <strsafe.h>
#include <windows.h>
#include <thread>
#include <list>
#include <utility>
#include <vector>
#include <iostream>

#define DO_REDIRECT_STDERR_OF_CHILD_PROCESS_TO_STDOUT_OF_CURRENT_PROCESS

// Format a readable error message, display a message box, and exit from the application.
void ErrorExit(const TCHAR* lpszFunction)
{
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;
    DWORD dw = GetLastError();

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER |
        FORMAT_MESSAGE_FROM_SYSTEM |
        FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR)&lpMsgBuf,
        0, NULL);

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT,
        (lstrlen((LPCTSTR)lpMsgBuf) + lstrlen((LPCTSTR)lpszFunction) + 40) * sizeof(TCHAR));
    StringCchPrintf((LPTSTR)lpDisplayBuf,
        LocalSize(lpDisplayBuf) / sizeof(TCHAR),
        TEXT("%s failed with error %d: %s"),
        lpszFunction, dw, lpMsgBuf);
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK);

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(1);
}


class CSubprocess
{
private:
    //The following members are used HANDLES
    HANDLE m_hChildStd_IN_Rd = nullptr;
    HANDLE m_hChildStd_IN_Wr = nullptr;
    HANDLE m_hChildStd_OUT_Rd = nullptr;
    HANDLE m_hChildStd_OUT_Wr = nullptr;

    // 
    wchar_t* m_cmd_as_wchar = nullptr;
    bool m_is_stdin_pipe = false;
    bool m_is_stdout_pipe = false;
    PROCESS_INFORMATION pi_Ret_ProcInfo;

    CSubprocess()
    {
    }

    ~CSubprocess()
    {
        if (m_cmd_as_wchar != nullptr)
        {
            delete[] m_cmd_as_wchar;
        }
    }

    // Create a child process that uses the previously created pipes for STDIN and STDOUT.
    bool createChildProcess()
    {
        // TCHAR szCmdline[] = TEXT("child");
        PROCESS_INFORMATION piProcInfo;
        STARTUPINFO siStartInfo;
        BOOL bSuccess = FALSE;

        // Set up members of the PROCESS_INFORMATION structure. 

        ZeroMemory(&piProcInfo, sizeof(PROCESS_INFORMATION));

        // Set up members of the STARTUPINFO structure. 
        // This structure specifies the STDIN and STDOUT handles for redirection.

        ZeroMemory(&siStartInfo, sizeof(STARTUPINFO));
        siStartInfo.cb = sizeof(STARTUPINFO);
        siStartInfo.hStdError = m_hChildStd_OUT_Wr;
        siStartInfo.hStdOutput = m_hChildStd_OUT_Wr;
        siStartInfo.hStdInput = m_hChildStd_IN_Rd;
        siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

        /*** Tuan Hoang ***/
#ifdef DO_REDIRECT_STDERR_OF_CHILD_PROCESS_TO_STDOUT_OF_CURRENT_PROCESS
        siStartInfo.hStdError = GetStdHandle(STD_OUTPUT_HANDLE);  // Redirect stderr of child process to stdout of current (parent) process (for testing).
#endif
        /*** Tuan Hoang ***/

        // Create the child process. 
        bSuccess = CreateProcess(NULL,
            m_cmd_as_wchar,    // command line
            NULL,              // process security attributes 
            NULL,              // primary thread security attributes 
            TRUE,              // handles are inherited
            0,                 // creation flags 
            NULL,              // use parent's environment 
            NULL,              // use parent's current directory 
            &siStartInfo,      // STARTUPINFO pointer 
            &piProcInfo);      // receives PROCESS_INFORMATION 

        // If an error occurs, exit the application. 
        if (!bSuccess)
        {
            //ErrorExit(TEXT("CreateProcess"));
            return false;
        }
        else
        {
            // Close handles to the child process and its primary thread.
            // Some applications might keep these handles to monitor the status
            // of the child process, for example. 
            //CloseHandle(piProcInfo.hProcess);
            //CloseHandle(piProcInfo.hThread);

            //pass piProcInfo to the returned process
            pi_Ret_ProcInfo = piProcInfo;

            // Close handles to the stdin and stdout pipes no longer needed by the child process.
            // If they are not explicitly closed, there is no way to recognize that the child process has ended.
            CloseHandle(m_hChildStd_OUT_Wr);
            CloseHandle(m_hChildStd_IN_Rd);

            return true;
        }
    }



public:
    // Executes child process with (optional) stdin and stdout pipes.
    // Return pointer to CSubprocess object in case of success, and nullptr in case of failure.
    // Remark: using std::wstring instead of std::string is not very useful here (cmd may contain unicode charcters).
    static CSubprocess* Popen(const std::wstring cmd, const bool is_stdin_pipe = false, const bool is_stdout_pipe = false, const int buf_size = 0)
    {
        CSubprocess* sp = new CSubprocess();

        sp->m_cmd_as_wchar = new wchar_t[cmd.length() + 1];
        wcscpy_s(sp->m_cmd_as_wchar, cmd.length() + 1, cmd.c_str());

        sp->m_is_stdin_pipe = is_stdin_pipe;
        sp->m_is_stdout_pipe = is_stdout_pipe;

        // Set the bInheritHandle flag so pipe handles are inherited. 
        SECURITY_ATTRIBUTES saAttr;
        saAttr.nLength = sizeof(SECURITY_ATTRIBUTES);
        saAttr.bInheritHandle = TRUE;
        saAttr.lpSecurityDescriptor = NULL;

        BOOL success;

        if (is_stdout_pipe)
        {
            // Create a pipe for the child process's STDOUT.
            success = CreatePipe(&sp->m_hChildStd_OUT_Rd, &sp->m_hChildStd_OUT_Wr, &saAttr, (DWORD)buf_size);

            if (!success)
            {
                // ErrorExit(TEXT("StdoutRd CreatePipe"));
                fprintf(stderr, "Error: StdoutRd CreatePipe\n");
                delete sp;
                return nullptr;
            }

            // Ensure the read handle to the pipe for STDOUT is not inherited.
            success = SetHandleInformation(sp->m_hChildStd_OUT_Rd, HANDLE_FLAG_INHERIT, 0);

            if (!success)
            {
                // ErrorExit(TEXT("StdoutRd CreatePipe"));
                fprintf(stderr, "Error: Stdout SetHandleInformation\n");
                delete sp;
                return nullptr;
            }
        }

        if (is_stdin_pipe)
        {
            // Create a pipe for the child process's STDIN.
            success = CreatePipe(&sp->m_hChildStd_IN_Rd, &sp->m_hChildStd_IN_Wr, &saAttr, (DWORD)buf_size);

            if (!success)
            {
                // ErrorExit(TEXT("Stdin CreatePipe"));
                fprintf(stderr, "Error: Stdin CreatePipe\n");
                delete sp;
                return nullptr;
            }

            // Ensure the write handle to the pipe for STDIN is not inherited.
            success = SetHandleInformation(sp->m_hChildStd_IN_Wr, HANDLE_FLAG_INHERIT, 0);

            if (!success)
            {
                // ErrorExit(TEXT("Stdin SetHandleInformation"));
                fprintf(stderr, "Error: Stdin SetHandleInformation\n");
                delete sp;
                return nullptr;
            }
        }

        // Create the child process.
        bool is_success = sp->createChildProcess();

        if (!is_success)
        {
            // If an error occurs, exit the application.
            delete sp;
            return nullptr;
        }

        return sp;
    }

    // Close sdtin PIPE and delete sp.
    static bool ClosePipeAndDeleteObj(CSubprocess* sp)
    {
        if (sp != nullptr)
        {
            if (sp->m_hChildStd_IN_Wr != nullptr)
            {
                BOOL success = CloseHandle(sp->m_hChildStd_IN_Wr);

                if (!success)
                {
                    return false;
                }
            }

            delete sp;
        }

        return true;
    }

    // Write to stdin PIPE and flush
    bool stdinWrite(const unsigned char* data_bytes, const unsigned int len)
    {
        DWORD dwWritten = 0;

        BOOL success = WriteFile(m_hChildStd_IN_Wr, (LPCVOID)data_bytes, (DWORD)len, &dwWritten, NULL);

        if (!success)
        {
            return false;
        }
        else
        {
            if (dwWritten != (DWORD)len)
            {
                fprintf(stderr, "len = %d, dwWritten = %d  Why???\n", len, (int)dwWritten);
            }
        }

        success = FlushFileBuffers(m_hChildStd_IN_Wr);

        if (!success)
        {
            fprintf(stderr, "FlushFileBuffers failed\n");
            return false;
        }

        return true;
    }

    // Read from stdout PIPE
    bool stdoutRead(const unsigned int len, unsigned char* data_bytes)
    {
        DWORD dwRead = 0;

        // The third argument of ReadFile is nNumberOfBytesToRead is the "The maximum number of bytes to be read."
        // We must use a loop for reading exactly <len> bytes from the pipe.
        int remain_len = (int)len;
        unsigned char* data_bytes_ptr = data_bytes;

        // Keep reading until finish reading <len> bytes from the PIPE.
        while (remain_len > 0)
        {
            BOOL success = ReadFile(m_hChildStd_OUT_Rd, data_bytes_ptr, (DWORD)remain_len, &dwRead, NULL);

            if (!success)
            {
                //close all handles
                //CloseHandle(pi_Ret_ProcInfo.hProcess);
                //CloseHandle(pi_Ret_ProcInfo.hThread);
                return false;
            }

            remain_len -= (int)dwRead;  // Subtract number of bytes read from remain_len
            data_bytes_ptr += dwRead;   // Advance pointer by number of bytes read.
        }

        //close all handles
        //CloseHandle(pi_Ret_ProcInfo.hProcess);
        //CloseHandle(pi_Ret_ProcInfo.hThread);
        return true;
    }

    // Close stdin PIPE
    // Note: there is some code duplication from ClosePipeAndDeleteObj function (but ClosePipeAndDeleteObj was [kind of] taken from Microsoft code sample, and just kept)
    bool stdinClose()
    {
        if (m_hChildStd_IN_Wr != nullptr)
        {
            BOOL success = CloseHandle(m_hChildStd_IN_Wr);

            m_hChildStd_IN_Wr = nullptr;    // Mark as "closed", even if not success.

            if (!success)
            {
                return false;
            }
        }

        return true;
    }

    // Get Process Information
    PROCESS_INFORMATION getProcessInformation()
    {
        return pi_Ret_ProcInfo;
    }
};

//Monitor the ffmpeg process that was executed by using callable function
void MonitorFfmpegPrrocess(PROCESS_INFORMATION pi, std::wstring filename)
{
    bool is_process_ended = false;
    for (; !is_process_ended;)
    {
        // Give some timeslice (50 ms), so we won't waste 100% CPU.
        is_process_ended = WaitForSingleObject(pi.hProcess, 50) == WAIT_OBJECT_0;
    }

    //Close the handles for PROCESS_INFORMATION pi
    //Close handles to the child process and its primary thread.

    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);

    //Pass the filename to NVIDIA FRUC library for interpolation
    //We will do this at step 2 later

}

//Convert MP4 to YUV
int SplitMp4ToYUV(std::wstring input_video ,int number_smaller_videos)
{
    std::vector<std::thread> ffmpeg_monitor_threads;
    std::vector<CSubprocess*> ffmpeg_processes;

    std::vector<std::pair<unsigned int, unsigned int>> timestamps{ std::make_pair(0, 1000) , std::make_pair(1000, 2000),
                            std::make_pair(2000, 3000), std::make_pair(3000, 4000),
                            std::make_pair(4000, 5000), std::make_pair(5000, 6000),
                            std::make_pair(6000, 7000) };


    std::vector<std::wstring> output_videos;
    for (int i = 0; i < number_smaller_videos; i++)
    {
        std::wstring trimmed_video_path = std::to_wstring(i + 1) + L"_trimedvideo.yuv";
        output_videos.push_back(trimmed_video_path);
    }

    for (int i = 0; i < number_smaller_videos; i++)
    {

        /*
        ffmpeg.exe -y -hwaccel_device 0 -hwaccel cuda -i inputVideoFile.mp4 -ss 1000ms -to 2000ms -pix_fmt nv12 1_trimedvideo.yuv
        */

        const std::wstring ffmpeg_cmd =
        L"ffmpeg.exe -y -hwaccel_device 0 -hwaccel cuda -i " + input_video + L" -ss " + std::to_wstring(timestamps[i].first) + L"ms -to " + std::to_wstring(timestamps[i].second) +
            L"ms -pix_fmt nv12 " + output_videos[i];

        //Create ffmpeg subprocess without stdin PIPE/stdout PIPE
        CSubprocess* ffmpeg_process = CSubprocess::Popen(ffmpeg_cmd);

        //Started ffmpeg process failed
        if (ffmpeg_process == nullptr)
        {
            //Exit the app
            ErrorExit(TEXT("CreateProcess ffmpeg_process"));
        }

        ffmpeg_processes.push_back(ffmpeg_process);

        std::thread ffmpeg_monitor_thread(MonitorFfmpegPrrocess, ffmpeg_process->getProcessInformation(), output_videos[i]);
        ffmpeg_monitor_threads.push_back(std::move(ffmpeg_monitor_thread));
    }

    for (int i = 0; i < number_smaller_videos; i++)
    {
        //completion of the ffmpeg_monitor_threads
        ffmpeg_monitor_threads[i].join();
    }

    //Close all handles and objs created by ffmpeg processes
    for (int i = 0; i < number_smaller_videos; i++)
    {
        bool success = CSubprocess::ClosePipeAndDeleteObj(ffmpeg_processes[i]);

        //Closing all handles and objs ffmpeg process has been failed
        if (!success)
        {
            //Exit the app
            ErrorExit(TEXT("ClosePipeAndDeleteObj CloseHandle"));
        }
    }
    return 0;
}

//Convert YUV to interpolate YUV using FRUC plugin
int InterpolateYUV(int number_smaller_videos, int width, int height)
{
    std::vector<std::thread> ffmpeg_monitor_threads;
    std::vector<CSubprocess*> ffmpeg_processes;

    //Input video path
    std::vector<std::wstring> input_videos;
    for (int i = 0; i < number_smaller_videos; i++)
    {
        std::wstring trimmed_video_path = std::to_wstring(i + 1) + L"_trimedvideo.yuv";
        input_videos.push_back(trimmed_video_path);
    }

    //std::vector<unsigned int> gpu_devices_indexes{ 0, 1, 0 , 1, 0, 1, 0 };

    std::vector<std::wstring> output_videos;
    for (int i = 0; i < number_smaller_videos; i++)
    {
        std::wstring trimmed_video_path = std::to_wstring(i + 1) + L"_interpolate_trimedvideo.yuv";
        output_videos.push_back(trimmed_video_path);
    }

    for (int i = 0; i < number_smaller_videos; i++)
    {

        /*
            ffmpeg.exe -y -hwaccel_device 0 -hwaccel cuda -hwaccel_output_format cuda -vsync 0 -pix_fmt nv12 -s 1920x1080 -i input.yuv -filter_complex "[0:v]hwupload_cuda[q];[q]nvoffruc[o2];[o2]hwdownload,format=nv12[w]" -map "[w]" -c:v rawvideo -f rawvideo democap.yuv
        */

        const std::wstring ffmpeg_cmd =

#if 1
            L"ffmpeg.exe -y -hwaccel_device 0 -hwaccel cuda -hwaccel_output_format cuda -vsync 0 -pix_fmt nv12 -s " +
            std::to_wstring(width) + L"x" + std::to_wstring(height) +
            L" -i " + input_videos[i] +
            L" -filter_complex \"[0:v]hwupload_cuda[q]; [q] nvoffruc=fps=240[o2]; [o2] hwdownload, format = nv12[w]\" -map \"[w]\" -c:v rawvideo -f rawvideo " +
            output_videos[i];
#endif        

        //Create ffmpeg subprocess without stdin PIPE/stdout PIPE
        CSubprocess* ffmpeg_process = CSubprocess::Popen(ffmpeg_cmd);

        //Started ffmpeg process failed
        if (ffmpeg_process == nullptr)
        {
            //Exit the app
            ErrorExit(TEXT("CreateProcess ffmpeg_process"));
        }

        ffmpeg_processes.push_back(ffmpeg_process);

        std::thread ffmpeg_monitor_thread(MonitorFfmpegPrrocess, ffmpeg_process->getProcessInformation(), output_videos[i]);
        //then push back this thread to ffmpeg_monitor_threads for monitoring later 
        ffmpeg_monitor_threads.push_back(std::move(ffmpeg_monitor_thread));
    }

    //when the interpolations finished - all ffmpeg_monitor_threads should have finished too
    for (int i = 0; i < number_smaller_videos; i++)
    {
        //completion of the ffmpeg_monitor_threads
        ffmpeg_monitor_threads[i].join();
    }

    //Close all handles and objs created by ffmpeg processes
    for (int i = 0; i < number_smaller_videos; i++)
    {
        bool success = CSubprocess::ClosePipeAndDeleteObj(ffmpeg_processes[i]);

        //Closing all handles and objs ffmpeg process has been failed
        if (!success)
        {
            //Exit the app
            ErrorExit(TEXT("ClosePipeAndDeleteObj CloseHandle"));
        }
    }
    return 0;
}

//Concatinate inpterpolate YUV to single YUV output
int ConcatYUV(int number_smaller_videos, int width, int height)
{
    std::vector<std::thread> ffmpeg_monitor_threads;
    std::vector<CSubprocess*> ffmpeg_processes;

    //Input video path
    std::cout << number_smaller_videos << std::endl;
    std::wstring input_video = L"";
    for (int i = 1; i <= number_smaller_videos; i++)
    {
        input_video = input_video + std::to_wstring(i) + L"_interpolate_trimedvideo.yuv";
        if (i != number_smaller_videos)
            input_video = input_video + L"|";
    }


    //std::vector<unsigned int> gpu_devices_indexes{ 0, 1, 0 , 1, 0, 1, 0 };

    std::wstring output_video = L"interpolate_output.yuv";

    /*
            ./ffmpeg -pix_fmt nv12 -s 1920x1080 -i "concat:1_interpolate_trimedvideo.yuv|3_interpolate_trimedvideo.yuv|5_interpolate_trimedvideo.yuv|7_interpolate_trimedvideo.yuv|2_interpolate_trimedvideo.yuv|4_interpolate_trimedvideo.yuv|6_interpolate_trimedvideo.yuv" -c copy output.yuv
    */

    const std::wstring ffmpeg_cmd =

#if 1
        L"ffmpeg.exe -pix_fmt nv12 -s " +
        std::to_wstring(width) + L"x" + std::to_wstring(height) +
        L" -y -i \"concat:" + input_video +
        L"\" -c copy " + output_video;
#endif        

    //Create ffmpeg subprocess without stdin PIPE/stdout PIPE
    CSubprocess* ffmpeg_process = CSubprocess::Popen(ffmpeg_cmd);

    //Started ffmpeg process failed
    if (ffmpeg_process == nullptr)
    {
        //Exit the app
        ErrorExit(TEXT("CreateProcess ffmpeg_process"));
    }

    //then push back this process to ffmpeg_process for monitoring later 
    ffmpeg_processes.push_back(ffmpeg_process);


    std::thread ffmpeg_monitor_thread(MonitorFfmpegPrrocess, ffmpeg_process->getProcessInformation(), output_video);
    //then push back this thread to ffmpeg_monitor_threads for monitoring later 
    ffmpeg_monitor_threads.push_back(std::move(ffmpeg_monitor_thread));


    ffmpeg_monitor_threads[0].join();

    bool success = CSubprocess::ClosePipeAndDeleteObj(ffmpeg_processes[0]);

    //Closing all handles and objs ffmpeg process has been failed
    if (!success)
    {
        //Exit the app
        ErrorExit(TEXT("ClosePipeAndDeleteObj CloseHandle"));
    }
    return 0;
}

//Convert output YUV to MP4
int ConvertToMP4(int width, int height)
{
    std::vector<std::thread> ffmpeg_monitor_threads;
    std::vector<CSubprocess*> ffmpeg_processes;

    std::wstring input_video = L"interpolate_output.yuv";
    std::wstring output_video = L"interpolate_output.mp4";

    /*
        ffmpeg.exe -y -vsync 0 -pix_fmt nv12 -s 1920x1080 -i output.yuv -filter_complex "[0:v]hwupload_cuda[o1]" -map "[o1]" -c:v h264_nvenc -b:v 8M output1.mp4
    */

    const std::wstring ffmpeg_cmd =

#if 1
        L"ffmpeg.exe -y -pix_fmt nv12 -s " +
        std::to_wstring(width) + L"x" + std::to_wstring(height) +
        L" -framerate 240 -i " + input_video +
        L" -filter_complex \"[0:v]hwupload_cuda[o1]\" -map \"[o1]\" -c:v h264_nvenc -b:v 8M " + output_video;
#endif        

    //Create ffmpeg subprocess without stdin PIPE/stdout PIPE
    CSubprocess* ffmpeg_process = CSubprocess::Popen(ffmpeg_cmd);

    //Started ffmpeg process failed
    if (ffmpeg_process == nullptr)
    {
        //Exit the app
        ErrorExit(TEXT("CreateProcess ffmpeg_process"));
    }

    //ffmpeg process launched successfuly
    //then push back this process to ffmpeg_process for monitoring later 
    ffmpeg_processes.push_back(ffmpeg_process);


    std::thread ffmpeg_monitor_thread(MonitorFfmpegPrrocess, ffmpeg_process->getProcessInformation(), output_video);
    //then push back this thread to ffmpeg_monitor_threads for monitoring later 
    ffmpeg_monitor_threads.push_back(std::move(ffmpeg_monitor_thread));


    ffmpeg_monitor_threads[0].join();

    bool success = CSubprocess::ClosePipeAndDeleteObj(ffmpeg_processes[0]);

    //Closing all handles and objs ffmpeg process has been failed
    if (!success)
    {
        //Exit the app
        ErrorExit(TEXT("ClosePipeAndDeleteObj CloseHandle"));
    }
    return 0;
}

int main()
{
    //Input video path
    std::wstring input_video = L"D:\\01_FRUCTask\\DEMOAPP\\DemoApp\\x64\\Debug\\Sample_Input.mp4";

    SplitMp4ToYUV(input_video, 7);
    InterpolateYUV(7, 1920, 1080);
    ConcatYUV(7, 1920, 1080);
    ConvertToMP4(1920, 1080);
}
