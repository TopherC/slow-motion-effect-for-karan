
# DemoApp

This Application built on VS_2022. it is a example to interpolate video using ffmpeg FRUC plugin.

## Requirements
- Visual Studio 2022
- Cuda Toolkit >= 11.6 
- FRUC DLLs (already copied in solution)

##Steps
- first install above requirements and then build the DemoApp Application
- solution file is in DEMOAPP\DemoApp location
- FFmpeg binary is in DEMOAPP\DemoApp\x64\Debug location

